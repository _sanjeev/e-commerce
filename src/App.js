import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from "./Components/Nav";
import Product from "./Components/Product";
import Homepage from "./Components/Homepage";
import Categories from "./Components/Categories";
import Announcement from "./Components/Announcement";
import WomenCategories from "./Components/WomenCategories";
import MenCategories from "./Components/MenCategories";
import JewelleryCategories from "./Components/JewelleryCategories";
import Gadgets from "./Components/Gadgets";
import AboutUs from "./Components/AboutUs";
import OurWork from "./Components/OurWork";
import ShowAllProduct from "./Components/ShowAllProduct";
import Start from "./Components/Start";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      productArr: [],
      mens: [],
      isDataFetched: false,
      error: false,
      loadingData: true,
    };
  }

  

  componentDidMount = () => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((data) => {
        const men = data.filter((key) => {
          return key.category === "men's clothing";
        })
        this.setState({
          productArr: data,
          mens: men,
          isDataFetched: true,
          loadingData: false,
          error: false,
        });
      })
      .catch((err) => {
        this.setState({
          error: true,
          loadingData: false,
        });
      });
  };

  render() {
    return (
      <>
        <Announcement />

        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route path="/products" element={<ShowAllProduct />} />
          <Route path="/collections" element={<Categories />} />
          <Route path="/men-categories" element={<MenCategories data = {this.state.mens} />} />
          <Route path="/women-categories" element={<WomenCategories />} />
          <Route
            path="/jewellery-categories"
            element={<JewelleryCategories />}
          />
          <Route path="/electronics-categories" element={<Gadgets />} />
          <Route path="/aboutus" element={<AboutUs />} />
          <Route path="/ourwork" element={<OurWork />} />
        </Routes>
      </>
    );
  }
}

export default App;

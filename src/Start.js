import React from "react";
import Cssloader from "./Components/Cssloader";
import Nav from './Components/Nav'
import Homepage from "./Components/Homepage";
import JewelleryCategories from "./Components/JewelleryCategories";
class Start extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            productArr: [],
            isDataFetched: false,
            error: false,
            loadingData: true
        }
    }

    componentDidMount = () => {
        fetch('https://fakestoreapi.com/products')
            .then(res => res.json())
            .then(data => {
                console.log(data);
                this.setState({
                    productArr: data,
                    isDataFetched: true,
                    loadingData: false,
                    error: false
                })
            }).catch((err) => {
                this.setState({
                    error: true,
                    loadingData: false
                })
            })
    }
    render() {
        console.log("prod", this.state.productArr)
        return (
            <>
                <div>
                    {this.state.loadingData === true ? (
                        <Cssloader />
                    ) : (
                        <JewelleryCategories array={this.state.productArr} />
                    )}
                </div>


            </>
        )
    }


}


export default Start;












import React from 'react';
import Cssloader from "./Cssloader";
import SingleProduct from './SingleProduct';
import { Link } from 'react-router-dom'
import HomeNav from './HomeNav';
import Data from './Data';

class MenCategories extends React.Component {

    constructor(props) {
        super(props);
        console.log(this.props.data);
        this.state = {
            result: this.props.data,
        }
    }

    addItem = (products) => {
        fetch("https://fakestoreapi.com/products", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: "test product",
        price: "13.5",
        description: "lorem ipsum set",
        image: "https://i.pravatar.cc",
        category: "electronic",
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
          let allData = products;
          allData.push (data);
        this.setState({ result: allData });
      });

    }


    render() {

        

        return (
            <div>
                {this.state.result.map((key) => (
                    <div>
                        <p>{key.title}</p>
                        <img src={key.image}/>
                    </div>
                ))}

                {/* Adding Button */}
                <Data addData = {this.addItem} products={this.state.result}/>
            </div>
            )

    }

}
export default MenCategories;

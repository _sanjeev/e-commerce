import React, { Component } from "react";

export default class Data extends Component {
  constructor(props) {
    super(props);
  }

  addItem = () => {
      this.props.addData(this.props.products);
  }

  render() {
    return <div>
        <button onClick={this.addItem}>AddItem</button>
    </div>;
  }
}

import React from "react";
import './style.css'
import { Link } from 'react-router-dom'
import HomeNav from './HomeNav';
import Footer from "./Footer";
import Nav from "./Nav";


export default class Homepage extends React.Component {

    render() {
        return (
            <div className="home-container">
                <Nav />
                <div className="home-header">
                    <div className="company">
                        <span className="first">B</span><span className="second">A</span><span className="rest">RREL</span>
                    </div>

                    <HomeNav></HomeNav>
                </div>
                <div className="main-container">
                    <div className="backgroundImage">
                        <section className="sale-offer">
                            <div className="header">FLASH SALE</div>
                            <div className="percentage">UP TO 60% OFF</div>
                        </section>
                    </div>

                    <div className="content">
                        <div> Luxe staples, lovingly made in Australia. Barrel is an award winning sustainable clothing brand. Simple, stylish wardrobe essentials, designed with integrity and made to last. We make clothes for living in!</div>
                        <Link className="link" to="/products"> <button className="btn"> SHOP NOW</button> </Link>
                    </div>
                </div>

                <Footer />
            </div>
        )
    }
}
import React from 'react';
import './style.css'
import Cssloader from "./Cssloader";
import HomeNav from './HomeNav';
import { Link } from 'react-router-dom'
import NewProduct from './NewProduct';
import SingleProduct from './SingleProduct';
import MenCategories from './MenCategories';
import Start from './Start';


class Product extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            productArr: [],
            isDataFetched: false,
            error: false,
            loadingData: true
        }
    }

    componentDidMount = () => {
        fetch('https://fakestoreapi.com/products')
            .then(res => res.json())
            .then(data => {
                console.log(data);
                this.setState({
                    productArr: data,
                    isDataFetched: true,
                    loadingData: false,
                    error: false
                })
            }).catch((err) => {
                this.setState({
                    error: true,
                    loadingData: false
                })
            })
    }


    render() {
        console.log("state", this.state.productArr)
        // log

        return (
            <div>
                {this.state.loadingData === true ? (
                    <Cssloader />
                ) : (
                    <Start/>
                )}
            </div>
        );
        
    }

}
export default Product;
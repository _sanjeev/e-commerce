import React from "react";
import './style.css'

export default class Announcement extends React.Component {

    render() {
        return (
            <div className="announcement">
                FREE SHIPPING ON ORDERS OVER RS. 1000.
            </div>
        )
    }
}
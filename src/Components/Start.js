import React from "react";
import JwelleryCategories from "./JewelleryCategories";
import MenCategories from "./MenCategories";

class Start extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            productArr: [],
            isDataFetched: false,
            error: false,
            loadingData: true
        }
    }

    componentDidMount = () => {
        fetch('https://fakestoreapi.com/products')
            .then(res => res.json())
            .then(data => {
                this.setState({
                    productArr: data,
                    isDataFetched: true,
                    loadingData: false,
                    error: false
                })
            }).catch((err) => {
                this.setState({
                    error: true,
                    loadingData: false
                })
            })
    }
    render() {
        console.log('Start-component');
        console.log(this.state.productArr)
        return (
            <>
                <div>
                    <MenCategories  array = {this.state.productArr}/>
                </div>


            </>
        )
    }


}


export default Start;